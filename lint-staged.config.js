const prefix = (a) => (b) => a + b;

module.exports = {
  "*.hx": (files) => [
    `haxelib run formatter ${files.map(prefix(`-s `)).join(" ")}`,
    `git add ${files.join(" ")}`,
  ],
  "*.ts": [],
};
