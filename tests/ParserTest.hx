package tests;

import sys.io.File;
import utest.Test;
import utest.Assert;
import Parser;

class ParserTest extends utest.Test {
  public function test_nothing() {
    var input = [];
    var expected = [];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_int() {
    var input = ["17"];
    var expected = [];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_float() {
    var expected = [];
    var input = ["102.64"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_string_single_quote() {
    var expected = [];
    var input = [];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_string_double_quote() {
    var expected = [];
    var input = [];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_string_template() {
    var expected = [];
    var input = [];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_string_template_interpolation() {
    var expected = [];
    var input = ["`this is a template string ${", "15", "+", "12", "} and some stuff`"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_array() {
    var expected = [];
    var input = ["[", "17", ",", "5", "]"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_object() {
    var expected = [];
    var input = ["{", "a", ":", "1", ",", "b", ":", "65", "}"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_decl() {
    var expected = [];
    var input = ["var", "foo", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_int() {
    var expected = [];
    var input = ["var", "foo", "=", "17", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_float() {
    var expected = [];
    var input = ["var", "foo", "=", "12.01", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_string() {
    var expected = [];
    var input = ["var", "foo", "=", "'hi there'", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_array() {
    var expected = [];
    var input = ["var", "foo", "=", "[", "1", ",", "2", ",", "3", "]", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_object() {
    var expected = [];
    var input = ["var", "foo", "=", "{", "a", ":", "1", ",", "b", ":", "2", "}", ";"];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }

  public function test_sample_file() {
    var expected = [];
    var input = [
      "class", "A", "{", "}", "class", "B", "{", "}", "const", "things", "=",
      "[", "1", ",", "2", ",", "3", "]", "const", "thing", "=", "{", "type",
      ":", "\"some type\"", ",", "job", ":", "'other string'", ",", "name",
      ":", "`yet another string`", ",", "description", ":",
      "`a list of things ${", "things", "}`", ",", "ns", ":", "[", "1", ",",
      "2", ",", "3", "]", "}", "export", "class", "ExampleClass", "{",
      "thing", "=", "thing", "things", "=", "things", "constructor", "(",
      "private", "readonly", "a", ":", "A", ",", "private", "readonly", "b",
      ":", "B", ")", "{", "}", "}"
    ];
    var result = Parser.parse(input);
    Assert.same(expected, result);
  }
}
