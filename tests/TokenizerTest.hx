package tests;

import sys.io.File;
import utest.Test;
import utest.Assert;
import Tokenizer;

class TokenizerTest extends utest.Test {
  public function test_nothing() {
    var input = "";
    var expected = [];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_int() {
    var input = "17";
    var expected = ["17"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_float() {
    var input = "102.64";
    var expected = ["102.64"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_string_single_quote() {
    var input = "'this is a singlequoted string'";
    var expected = [input];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_string_double_quote() {
    var input = '"this is a doublequoted string"';
    var expected = [input];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_string_template() {
    var input = '`this is a template string`';
    var expected = [input];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_string_template_interpolation() {
    var input = "`this is a template string ${15 + 12} and some stuff`";
    var expected = ["`this is a template string ${", "15", "+", "12", "} and some stuff`"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_array() {
    var input = "[17 ,5]";
    var expected = ["[", "17", ",", "5", "]"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_object() {
    var input = "{a: 1, b: 65}";
    var expected = ["{", "a", ":", "1", ",", "b", ":", "65", "}"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_decl() {
    var input = "var foo;";
    var expected = ["var", "foo", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_int() {
    var input = "var foo = 17;";
    var expected = ["var", "foo", "=", "17", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_float() {
    var input = "var foo = 12.01;";
    var expected = ["var", "foo", "=", "12.01", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_string() {
    var input = "var foo = 'hi there';";
    var expected = ["var", "foo", "=", "'hi there'", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_array() {
    var input = "var foo = [1 ,2 ,3];";
    var expected = ["var", "foo", "=", "[", "1", ",", "2", ",", "3", "]", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_var_assign_object() {
    var input = "var foo = {a: 1, b: 2};";
    var expected = ["var", "foo", "=", "{", "a", ":", "1", ",", "b", ":", "2", "}", ";"];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }

  public function test_sample_file() {
    var input = File.getContent("./ts/ExampleClass.ts");
    var expected = [
      "class", "A", "{", "}", "class", "B", "{", "}", "const", "things", "=",
      "[", "1", ",", "2", ",", "3", "]", "const", "thing", "=", "{", "type",
      ":", "\"some type\"", ",", "job", ":", "'other string'", ",", "name",
      ":", "`yet another string`", ",", "description", ":",
      "`a list of things ${", "things", "}`", ",", "ns", ":", "[", "1", ",",
      "2", ",", "3", "]", "}", "export", "class", "ExampleClass", "{",
      "thing", "=", "thing", "things", "=", "things", "constructor", "(",
      "private", "readonly", "a", ":", "A", ",", "private", "readonly", "b",
      ":", "B", ")", "{", "}", "}"
    ];
    var result = Tokenizer.split(input);
    Assert.same(expected, result);
  }
}
