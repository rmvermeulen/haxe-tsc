package lexing;

import TsLexer;
import Data;

class LexerTest extends utest.Test {
  private function lex(input:String, ?filename:String) {
    var bytes = byte.ByteData.ofString(input);
    var lexer = new TsLexer(bytes, filename);
    var data = [];
    try {
      while (true) {
        var token:Token = lexer.token(TsLexer.ruleset);
        if (token.tok == Eof) {
          break;
        }
        data.push(token.tok);
      }
    } catch (error:TsLexer.LexerError) {
      trace(error.msg);
      trace(error.pos);
    }
    return data;
  }

  inline function cstring(s:String) {
    return Const(CString(s));
  }
}
