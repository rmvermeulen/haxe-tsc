package lexing;

import utest.Assert;

class Strings extends LexerTest {
  public function test_strings_word() {
    Assert.same(lex("'word'"), [cstring("word")]);
    Assert.same(lex("'word '"), [cstring("word ")]);

    Assert.same(lex('"word"'), [cstring("word")]);
    Assert.same(lex('"word "'), [cstring("word ")]);
  }

  public function test_strings_sentence() {
    Assert.same(lex("'hello, world!'"), [cstring("hello, world!")]);
    Assert.same(lex('"hello, world!"'), [cstring("hello, world!")]);
  }

  public function test_strings_comments() {
    Assert.same(lex("'# a comment in a string'"),
      [cstring("# a comment in a string")]);
    Assert.same(lex("'// a comment in a string'"),
      [cstring("// a comment in a string")]);

    Assert.same(lex('"# a comment in a string"'),
      [cstring("# a comment in a string")]);
    Assert.same(lex('"// a comment in a string"'),
      [cstring("// a comment in a string")]);
  }

  public function test_strings_special() {
    Assert.same(lex("'/a|regex|in|a|string/gi'"),
      [cstring("/a|regex|in|a|string/gi")]);

    Assert.same(lex('"/a|regex|in|a|string/gi"'),
      [cstring("/a|regex|in|a|string/gi")]);
  }

  public function test_strings_sq_in_sq() {
    // an escaped single-quote in a single qoutes
    Assert.same(lex("'\\''"), [cstring("'")]);
  }

  public function test_strings_dq_in_sq() {
    // a double-quote in single quotes
    Assert.same(lex("'\"'"), [cstring('"')]);
  }

  public function test_strings_sq_in_dq() {
    // a single-quote in a double quotes
    Assert.same(lex('"\'"'), [cstring("'")]);
  }

  public function test_strings_dq_in_dq() {
    // an escaped double-quote in a double quotes
    Assert.same(lex('"\\""'), [cstring('"')]);
  }
}
