package tests;

class TestAll {
  public static function main() {
    utest.UTest.run([new TokenizerTest(), new ParserTest(), new TsLexerTest(), new lexing.Strings()]);
  }
}
