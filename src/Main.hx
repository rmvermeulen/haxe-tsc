import sys.FileSystem;
import haxe.io.Error;
import sys.io.File;

using StringTools;
using UnicodeString;

class Main {
  var paths = [];

  public function new() {}

  public function add_path(path:String) {
    paths.push(path);
  }

  public function start() {
    trace('Paths: $paths');
    var contents = paths.filter(p -> FileSystem.exists(p))
      .map(p -> {path: p, text: File.getContent(p)})
      .map(data -> {
        path: data.path,
        text: data.text,
        tokens: tokenize(data.text)
      });

    for (data in contents) {
      trace(data);
    }
  }

  public static function main() {
    var main = new Main();
    Sys.println('Main created! $main');

    main.add_path("./ts/ExampleClass.ts");
    main.add_path("./ts/main.ts");
    main.add_path("./ts/module_loader.ts");

    Sys.println('Main running!');

    main.start();

    Sys.println("Main done");
  }

  private function tokenize(text:UnicodeString):Array<String> {
    var tokens = [];
    var current_token = "";

    var index = 0;
    while (index < text.length) {
      var c = text.charAt(index);
      if (c.isSpace(0)) {
        if (current_token != "") {
          tokens.push(current_token);
        }
        current_token = "";
      }
      current_token += c;
      index += 1;
    }

    return tokens;
  }
}
