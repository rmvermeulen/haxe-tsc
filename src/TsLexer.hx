import haxe.macro.Expr.Position;
import hxparse.Lexer;
import Data;

enum LexerErrorMsg {
  UnterminatedString;
  UnterminatedRegExp;
  UnclosedComment;
  UnterminatedEscapeSequence;
  InvalidEscapeSequence(c:String);
  UnknownEscapeSequence(c:String);
  UnclosedCode;
}

class LexerError {
  public var msg:LexerErrorMsg;
  public var pos:Position;

  public function new(msg, pos) {
    this.msg = msg;
    this.pos = pos;
  }
}

class TsLexer extends Lexer implements hxparse.RuleBuilder {
  // static var keywords = @:mapping Keyword;
  static var integer = "([1-9][0-9]*)|0";
  static var fraction = "\\.[0-9]+";
  static var hex_int = "0x[0-9a-fA-F]+";

  static var buffer = new StringBuf();

  public static var string_dq = @:rule [
    // match escaped quotes
    "\\\\\\\\" => {
      buffer.add("\\\\");
      lexer.token(string_dq);
    },
    "\\\\" => {
      buffer.add("\\");
      lexer.token(string_dq);
    },
    "\\\\\"" => {
      buffer.add('"');
      lexer.token(string_dq);
    },
    // match the closing quote
    '"' => {
      lexer.curPos().pmax;
    },
    // match anything not double-quote
    '[^\\"]+' => {
      buffer.add(lexer.current);
      lexer.token(string_dq);
    }
  ];

  public static var string_sq = @:rule [
    // match escaped quotes
    "\\\\\\\\" => {
      buffer.add("\\\\");
      lexer.token(string_sq);
    },
    "\\\\" => {
      buffer.add("\\");
      lexer.token(string_sq);
    },
    "\\\\'" => {
      buffer.add("'");
      lexer.token(string_sq);
    },
    // match the closing quote
    "'" => {
      lexer.curPos().pmax;
    },
    // match anything not single-quote
    "[^\\']+" => {
      buffer.add(lexer.current);
      lexer.token(string_sq);
    }
  ];

  public static var ruleset = @:rule [
    "" => mk(lexer, Eof),
    "{" => mk(lexer, BraceOpen),
    "}" => mk(lexer, BraceClose),
    "[" => mk(lexer, BracketOpen),
    "]" => mk(lexer, BracketClose),
    "\\(" => mk(lexer, ParenOpen),
    "\\)" => mk(lexer, ParenClose),
    ";" => mk(lexer, Semicolon),
    ":" => mk(lexer, Colon),
    "\\!" => mk(lexer, Bang),
    "\\+" => mk(lexer, BinaryOp(OpAdd)),
    "\\-" => mk(lexer, BinaryOp(OpSubtract)),
    "=" => mk(lexer, BinaryOp(OpEquals)),
    hex_int => mk(lexer, Const(CInt(lexer.current))),
    integer => mk(lexer, Const(CInt(lexer.current))),
    integer + fraction => mk(lexer, Const(CFloat(lexer.current))),
    "'" => {
      buffer = new StringBuf();
      var pmin = lexer.curPos();
      var pmax = try lexer.token(string_sq) catch (e:haxe.io.Eof)
        throw new LexerError(UnterminatedString, mkPos(pmin));
      var token = mk(lexer,
        Const(CString(unescape(buffer.toString(), mkPos(pmin)))));
      token.pos.min = pmin.pmin;
      token;
    },
    '"' => {
      buffer = new StringBuf();
      var pmin = lexer.curPos();
      var pmax = try lexer.token(string_dq) catch (e:haxe.io.Eof)
        throw new LexerError(UnterminatedString, mkPos(pmin));
      var token = mk(lexer,
        Const(CString(unescape(buffer.toString(), mkPos(pmin)))));
      token.pos.min = pmin.pmin;
      token;
    },
    // comment to force closing bracket no own line
  ];

  static function mkPos(p:hxparse.Position) {
    return {
      file: p.psource,
      min: p.pmin,
      max: p.pmax
    };
  }

  static function mk(lexer:Lexer, input:TokenDef) {
    return new Token(input, mkPos(lexer.curPos()));
  }

  static inline function unescapePos(pos:Position, index:Int, length:Int) {
    return {
      file: pos.file,
      min: pos.min + index,
      max: pos.min + index + length
    }
  }

  static function unescape(s:String, pos:Position) {
    var b = new StringBuf();
    var i = 0;
    var esc = false;
    while (true) {
      if (s.length == i) {
        break;
      }
      var c = s.charCodeAt(i);
      if (esc) {
        var iNext = i + 1;
        switch (c) {
          case 'n'.code:
            b.add("\n");
          case 'r'.code:
            b.add("\r");
          case 't'.code:
            b.add("\t");
          case '"'.code | '\''.code | '\\'.code:
            b.addChar(c);
          case _ >= '0'.code && _ <= '3'.code => true:
            iNext += 2;
          case 'x'.code:
            var chars = s.substr(i + 1, 2);
            if (!(~/^[0-9a-fA-F]{2}$/.match(chars)))
              throw new LexerError(InvalidEscapeSequence("\\x" + chars),
                unescapePos(pos, i, 1 + 2));
            var c = Std.parseInt("0x" + chars);
            b.addChar(c);
            iNext += 2;
          case 'u'.code:
            var c:Int;
            if (s.charAt(i + 1) == "{") {
              var endIndex = s.indexOf("}", i + 3);
              if (endIndex == -1)
                throw new LexerError(UnterminatedEscapeSequence,
                  unescapePos(pos, i, 2));
              var l = endIndex - (i + 2);
              var chars = s.substr(i + 2, l);
              if (!(~/^[0-9a-fA-F]+$/.match(chars)))
                throw new LexerError(InvalidEscapeSequence("\\u{"
                  + chars
                  + "}"),
                  unescapePos(pos, i, 1 + 2 + l));
              c = Std.parseInt("0x" + chars);
              if (c > 0x10FFFF)
                throw new LexerError(InvalidEscapeSequence("\\u{"
                  + chars
                  + "}"),
                  unescapePos(pos, i, 1 + 2 + l));
              iNext += 2 + l;
            } else {
              var chars = s.substr(i + 1, 4);
              if (!(~/^[0-9a-fA-F]{4}$/.match(chars)))
                throw new LexerError(InvalidEscapeSequence("\\u" + chars),
                  unescapePos(pos, i, 1 + 4));
              c = Std.parseInt("0x" + chars);
              iNext += 4;
            }
            b.addChar(c);
          case c:
            throw new LexerError(UnknownEscapeSequence("\\"
              + String.fromCharCode(c)),
              unescapePos(pos, i, 1));
        }
        esc = false;
        i = iNext;
      } else
        switch (c) {
          case '\\'.code:
            ++i;
            esc = true;
          case _:
            b.addChar(c);
            ++i;
        }
    }
    return b.toString();
  }
}
