using StringTools;

enum Mode {
  Default;
  String(prevMode:Mode, singleQuote:Bool);
  StringTemplate(prevMode:Mode, escaped:Bool, postDollar:Bool);
  StringTemplateInput(prevMode:Mode);
}

class Tokenizer {
  public static function split(text:String) {
    var mode:Mode = Mode.Default;

    var tokens = [];
    var current_token = "";

    var finishToken = (terminator:String = "") -> {
      current_token += terminator;
      if (current_token != "") {
        tokens.push(current_token);
        current_token = "";
      }
    }

    for (i in 0...text.length) {
      var c = text.charAt(i);
      var takeChar = () -> current_token += c;
      switch (mode) {
        case Mode.Default:
          if (c.isSpace(0)) {
            finishToken();
            continue;
          }
          switch (c) {
            case "`":
              finishToken();
              takeChar();
              mode = Mode.StringTemplate(mode, false, false);

            case "'", "\"":
              finishToken();
              takeChar();
              mode = Mode.String(mode, c == "'");

            case "@", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "{",
              "}", "[", "]", "/", "\\", ":", ";", ",", "<", ">":
              finishToken();
              tokens.push(c);

            case _:
              takeChar();
          }

        case Mode.String(prevMode, singleQuote):
          var terminator = singleQuote ? "'" : '"';
          takeChar();
          // if (~/\r\n/.match(c)) {}else
          if (c == terminator) {
            finishToken();
            mode = prevMode;
          }

        case Mode.StringTemplate(prevMode, escaped, postDollar):
          if (escaped) {
            // do the escaping
          } else {
            switch (c) {
              case "`":
                finishToken(c);
                mode = prevMode;

              case "$":
                if (postDollar) {
                  takeChar();
                } else {
                  mode = Mode.StringTemplate(prevMode, false, true);
                }

              case "{":
                if (postDollar) {
                  finishToken("${");
                  mode = Mode.StringTemplateInput(mode);
                } else {
                  takeChar();
                }

              case _:
                takeChar();
            }
          }

        case Mode.StringTemplateInput(prevMode):
          if (c.isSpace(0)) {
            finishToken();
            continue;
          }
          switch (c) {
            case "}", "`":
              finishToken();
              takeChar();
              mode = prevMode;

            case "'", "\"":
              finishToken();
              takeChar();
              mode = Mode.String(mode, c == "'");

            case "@", "$", "%", "^", "&", "*", "(", ")", "-", "+", "=", "{",
              // "}",
              "[", "]", "/", "\\", ":", ";", ",", "<", ">":
              finishToken();
              tokens.push(c);

            case _:
              takeChar();
          }
      }
    }
    finishToken();
    return tokens;
  }
}
