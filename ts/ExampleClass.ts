class A { }
class B { }

const things = [1, 2, 3]
const thing = {
    type: "some type",
    job: 'other string',
    name: `yet another string`,
    description: `a list of things ${things}`,
    ns: [1, 2, 3]
}
export class ExampleClass {
    thing = thing
    things = things
    constructor(private readonly a: A, private readonly b: B) { }

}


